﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YearGenerator
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Input number: ");
                var year = Console.ReadLine();
                if (year.Length > 15)
                {
                    Console.WriteLine("Out of range, please input less number");
                    continue;
                }
                if (int.TryParse(year, out var result))
                {
                    Console.WriteLine(YearGenerator(year));
                }
                else
                {
                    continue;
                }
            }
        }

        static private string YearGenerator(string year)
        {
            var yearString = "";
            var flagThreeSeparator = false;
            if (year.Length > 1)
            {
                for (var index = 0; index < year.Length; index++)
                {
                    var yearNum = int.Parse(year.Substring(index, 1));
                    

                    if (IsNumberOneAboveTen(yearNum, index, year.Length))
                    {
                        //(length - (index + 1)) % 3 == 1 //puluhan
                        
                        if (index == year.Length - 2)
                        {
                            yearNum = int.Parse(year.Substring(index, 2));
                            yearString += GetNumberBelasString(yearNum);
                            break;
                        }
                        if ((year.Length - (index + 1)) % 3 == 1)
                        {
                            yearNum = int.Parse(year.Substring(index, 2));
                            index++;
                            yearString += $"{GetNumberBelasString(yearNum)} { GetNumberSeparatorStringV2(index, year.Length)}";
                            
                        }
                        else
                        {
                            yearString += $"SE{GetNumberSeparatorStringV2(index, year.Length)}";
                        }
                        flagThreeSeparator = false;
                    }
                    else if (yearNum != 0)
                    {
                        yearString += $"{GetNumberString(yearNum)} {GetNumberSeparatorStringV2(index, year.Length)}";
                        
                    }
                    else if((year.Length - (index)) % 3 == 1 && index != year.Length - 2 && flagThreeSeparator == false && !string.IsNullOrEmpty(yearString))
                    {
                        yearString += $"{GetNumberSeparatorStringV2(index, year.Length)}";
                        flagThreeSeparator = true;
                    }

                    //ADD ON
                    if (index + 1 < year.Length && yearNum != 0)
                    {
                        yearString += " ";
                    }
                    if ((year.Length - (index)) % 3 == 1)
                    {
                        flagThreeSeparator = true;
                    }
                }
            }
            else if (year.Length == 1)
            {

                var yearNum = int.Parse(year.Substring(0, 1));

                yearString += GetNumberString(yearNum);


            }
            return yearString;
        }

        static private bool IsNumberOneAboveTen(int number, int index, int length)
        {

            var numb = length - (index + 1);
            if (number == 1)
            {
                if (numb % 3 == 1 || numb % 3 == 2)
                {
                    return true;
                }
                else if (numb == 3 && length == 4)
                {
                    //thousand
                    return true;
                }
                else if (numb == 3 && length > 4)
                {
                    //thousand
                    return false;
                }
                else if (numb == 6)
                {
                    //million
                    return false;
                }
                else if (numb == 9)
                {
                    //billion
                    return false;
                }
                else if (numb == 12)
                {
                    //trillion
                    return false;
                }
            }

            return false;
        }

        /// <summary>
        /// Get string of year
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        static private string GetNumberString(int number)
        {
            switch (number)
            {
                case 1:
                    return "SATU";
                case 2:
                    return "DUA";
                case 3:
                    return "TIGA";
                case 4:
                    return "EMPAT";
                case 5:
                    return "LIMA";
                case 6:
                    return "ENAM";
                case 7:
                    return "TUJUH";
                case 8:
                    return "DELAPAN";
                case 9:
                    return "SEMBILAN";
                default: return "";
            }
        }

        /// <summary>
        /// Get string of year for last 2 digit starting with 11-19
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        static private string GetNumberBelasString(int number)
        {
            switch (number)
            {
                case 10:
                    return "SEPULUH";
                case 11:
                    return "SEBELAS";
                case 12:
                    return "DUA BELAS";
                case 13:
                    return "TIGA BELAS";
                case 14:
                    return "EMPAT BELAS";
                case 15:
                    return "LIMA BELAS";
                case 16:
                    return "ENAM BELAS";
                case 17:
                    return "TUJUH BELAS";
                case 18:
                    return "DELAPAN BELAS";
                case 19:
                    return "SEMBILAN BELAS";
                default: return "";
            }
        }

        /// <summary>
        /// Get year separator up to thousand
        /// </summary>
        /// <param name="digit"></param>
        /// <returns></returns>
        static private string GetYearSeparatorStringV1(int digit)
        {
            switch (digit)
            {
                case 0:
                    return "RIBU";
                case 1:
                    return "RATUS";
                case 2:
                    return "PULUH";
                default: return "";
            }
        }

        /// <summary>
        /// Get number separator up to trillion
        /// </summary>
        /// <param name="index"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        static private string GetNumberSeparatorStringV2(int index, int length)
        {
            var numb = length - (index + 1);

            if (numb % 3 == 2)
            {
                return "RATUS";
            }
            else if (numb % 3 == 1)
            {
                return "PULUH";
            }
            else if (numb == 3)
            {
                //digit 4
                return "RIBU";
            }
            else if (numb == 6)
            {
                //digit 7
                return "JUTA";
            }
            else if (numb == 9)
            {
                //digit 10
                return "MILIAR";
            }
            else if (numb == 12)
            {
                //digit 13
                return "TRILIUN";
            }
            return "";
        }
    }
}
